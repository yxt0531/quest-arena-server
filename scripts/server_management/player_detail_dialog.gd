extends WindowDialog

var selected_player_id

func _ready():
	selected_player_id = -1

func _process(delta):
	if get_parent().get_node("PlayerList").selected_id_text != "":
		if NetworkPeerController.players.has(int(get_parent().get_node("PlayerList").selected_id_text)):
			selected_player_id = int(get_parent().get_node("PlayerList").selected_id_text)
			
			$Nickname.text = str(NetworkPeerController.players[selected_player_id].nickname)
			$CurrentMap.text = str(NetworkPeerController.players[selected_player_id].current_map)
			
			$WeaponAccentColor.text = _get_color_name_from_idx(NetworkPeerController.players[selected_player_id].weapon_accent_color)
			$WeaponLaserColor.text = _get_color_name_from_idx(NetworkPeerController.players[selected_player_id].weapon_laser_color)
			
			$AvatarPrimaryColor.text = _get_color_name_from_idx(NetworkPeerController.players[selected_player_id].avatar_primary_color)
			$AvatarSecondaryColor.text = _get_color_name_from_idx(NetworkPeerController.players[selected_player_id].avatar_secondary_color)
			$AvatarAccentColor.text = _get_color_name_from_idx(NetworkPeerController.players[selected_player_id].avatar_accent_color)
			$AvatarEmissiveColor.text = _get_color_name_from_idx(NetworkPeerController.players[selected_player_id].avatar_emissive_color)
	
			$Translation.text = str(NetworkPeerController.players[selected_player_id].translation)
			$RDegs.text = str(NetworkPeerController.players[selected_player_id].rotation_degrees)
			
			$HBTranslation.text = str(NetworkPeerController.players[selected_player_id].hitbox_translation)
			$HBRDegs.text = str(NetworkPeerController.players[selected_player_id].hitbox_rotation_degrees)
			
			$LeftWeapon.text = _get_weapon_name_from_idx(NetworkPeerController.players[selected_player_id].left_weapon)
			$LWTranslation.text = str(NetworkPeerController.players[selected_player_id].left_weapon_translation)
			$LWRDegs.text = str(NetworkPeerController.players[selected_player_id].left_weapon_rotation_degrees)
			$LWLaserOn.text = str(NetworkPeerController.players[selected_player_id].left_weapon_laser_on)
			
			$RightWeapon.text = _get_weapon_name_from_idx(NetworkPeerController.players[selected_player_id].right_weapon)
			$RWTranslation.text = str(NetworkPeerController.players[selected_player_id].right_weapon_translation)
			$RWRDegs.text = str(NetworkPeerController.players[selected_player_id].right_weapon_rotation_degrees)
			$RWLaserOn.text = str(NetworkPeerController.players[selected_player_id].right_weapon_laser_on)
	else:
		selected_player_id = -1
		
func _on_DisconnectPlayer_button_down():
	if selected_player_id != -1:
		NetworkPeerController.disconnect_player(selected_player_id)
	hide()

func _get_color_name_from_idx(index):
	match index:
		0:
			return("Aqua")
		1:
			return("Black")
		2:
			return("Blue")
		3:
			return("Fuchsia")
		4:
			return("Gray")
		5:
			return("Green")
		6:
			return("Lime")
		7:
			return("Maroon")
		8:
			return("Navy Blue")
		9:
			return("Purple")
		10:
			return("Red")
		11:
			return("Silver")
		12:
			return("Teal")
		13:
			return("White")
		14:
			return("Yellow")
		_:
			return("Null")

func _get_weapon_name_from_idx(index):
	match index:
		0:
			return("Handgun")
		1:
			return("Shotgun")
		2:
			return("SMG")
		_:
			return("Null")
