extends RichTextLabel

var current_datetime

func _ready():
	NetworkPeerController.logger = self
	UPNPRequester.logger = self
	current_datetime = OS.get_datetime()
	
func append(new_text):
	if NetworkPeerController.logging:
		current_datetime = OS.get_datetime()
		add_text(str(current_datetime.year) + "/" + str(current_datetime.month) + "/" + str(current_datetime.day) + " " + str(current_datetime.hour) + ":" + str(current_datetime.minute) + ":" + str(current_datetime.second) + " " + new_text + "\n")
