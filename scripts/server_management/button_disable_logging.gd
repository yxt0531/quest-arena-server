extends Button

func _ready():
	if NetworkPeerController.logging:
		text = "Disable Logging"
	else:
		text = "Enable Logging"

func _on_DisableLogging_button_down():
	NetworkPeerController.logging = not NetworkPeerController.logging
	if NetworkPeerController.logging:
		text = "Disable Logging"
	else:
		text = "Enable Logging"
	NetworkPeerController.save()
