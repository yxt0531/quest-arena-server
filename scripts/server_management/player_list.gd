extends ItemList

var selected_id_text

func _ready():
	selected_id_text = ""

func _process(delta):
	refresh()

func _get_item_index(item):
	var current_idx = 0
	while current_idx < get_item_count():
		if item == get_item_text(current_idx):
			return current_idx
		current_idx += 1
	return -1

func _get_items():
	var items = []
	var current_idx = 0
	while current_idx < get_item_count():
		items.append(get_item_text(current_idx))
		current_idx += 1
	return items

func refresh():
	# add new id to list
	for player_id in NetworkPeerController.players.keys():
		if _get_item_index(str(player_id)) == -1:
			add_item(str(player_id))
			print((str(player_id) + " added to player list"))
			
	# remove orphaned id from list
	for player_id_text in _get_items():
		if not NetworkPeerController.players.has(int(player_id_text)):
			remove_item(_get_item_index(player_id_text))
			_on_PlayerList_nothing_selected()
			print(player_id_text + " removed from player list")

func _on_PlayerList_item_selected(index):
	selected_id_text = get_item_text(index)
	get_parent().get_node("PlayerDetailDialog").popup_centered()

func _on_PlayerList_nothing_selected():
	selected_id_text = ""
