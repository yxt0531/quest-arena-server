extends Button

func _ready():
	for map_name in Map.maps.keys():
		get_parent().get_node("MapList/Maps").add_item(map_name)
	get_parent().get_node("MapList/Maps").sort_items_by_text()

func _on_ChangeMap_button_down():
	get_parent().get_node("MapList").popup_centered()

func _on_Maps_item_selected(index):
	print(Map.maps[get_parent().get_node("MapList/Maps").get_item_text(index)] + " selected")
	
	NetworkPeerController.current_map = Map.maps[get_parent().get_node("MapList/Maps").get_item_text(index)]
	print("network peer controller current map set: " + Map.maps[get_parent().get_node("MapList/Maps").get_item_text(index)])
	NetworkPeerController.map_changed()
	NetworkPeerController.logger.append("current map changed: " + Map.maps[get_parent().get_node("MapList/Maps").get_item_text(index)])
	get_parent().get_node("MapList").hide()
