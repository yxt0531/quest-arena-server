extends Control

func _ready():
	if name == "InitSetup":
		if OS.get_name() == "Android":
			get_tree().change_scene("res://scenes/init_setup_android.tscn")
			return
	
	for map_name in Map.maps.keys():
		$MapList.add_item(map_name)
	
	$MapList.sort_items_by_text()
	
	$LabelNotification.text = ""
	
	$LabelPort/InputPort.text = str(NetworkPeerController.port)
	$LabelMaxPlayer/InputMaxPlayer.text = str(NetworkPeerController.max_player)
	$LabelDamageMultiplier/InputDamageMultiplier.text = str(NetworkPeerController.damage_multiplier)
	
	if UPNPRequester.is_supported:
		$EnableUPnP.disabled = false
	else:
		$EnableUPnP.text = "UPnP Not Supported"
	_is_start_ready()

func _is_start_ready():
	if not _is_port_valid():
		$LabelNotification.text = "Server port must be between 1024 and 65535."
		$ButtonStart.disabled = true
	elif not _is_max_player_valid():
		$LabelNotification.text = "Max player count must be between 2 and 16."
		$ButtonStart.disabled = true
	elif not _is_map_selected():
		$LabelNotification.text = "Select a map from the map list."
		$ButtonStart.disabled = true
	elif not _is_damage_multiplier_valid():
		$LabelNotification.text = "Damage multiplier must be greater than 0."
		$ButtonStart.disabled = true
	else:
		$LabelNotification.text = "Server ready to start."
		$ButtonStart.disabled = false

func _is_port_valid():
	if Validator.is_number($LabelPort/InputPort.text):
		if int($LabelPort/InputPort.text) < 1024 or int($LabelPort/InputPort.text) > 65535:
			return false
		else:
			return true
	else:
		return false

func _is_max_player_valid():
	if Validator.is_number($LabelMaxPlayer/InputMaxPlayer.text):
		if int($LabelMaxPlayer/InputMaxPlayer.text) < 2 or int($LabelMaxPlayer/InputMaxPlayer.text) > 16:
			return false
		else:
			return true
	else:
		return false

func _is_map_selected():
	for map in Map.maps.values():
		if NetworkPeerController.current_map == map:
			return true
	
	return false

func _is_damage_multiplier_valid():
	if Validator.is_float($LabelDamageMultiplier/InputDamageMultiplier.text):
		if float($LabelDamageMultiplier/InputDamageMultiplier.text) <= 0:
			return false
		else:
			return true
	else:
		return false

func _on_InputPort_text_changed(new_text):
	if not Validator.is_number(new_text):
		$LabelPort/InputPort.text = ""
	_is_start_ready()

func _on_InputMaxPlayer_text_changed(new_text):
	if not Validator.is_number(new_text):
		$LabelMaxPlayer/InputMaxPlayer.text = ""
	_is_start_ready()

func _on_ButtonStart_button_down():
	print("starting network peer controller")
	
	NetworkPeerController.port = int($LabelPort/InputPort.text)
	NetworkPeerController.max_player = int($LabelMaxPlayer/InputMaxPlayer.text)
	NetworkPeerController.damage_multiplier = float($LabelDamageMultiplier/InputDamageMultiplier.text)
	
	if $EnableUPnP.pressed:
		UPNPRequester.activate_port_forwarding(NetworkPeerController.port)
	
	NetworkPeerController.start()
	
	if OS.get_name() == "Android":
		get_tree().change_scene("res://scenes/server_management_android.tscn")
		return
	
	get_tree().change_scene("res://scenes/server_management.tscn")

func _on_MapList_item_selected(index):
	print(Map.maps[$MapList.get_item_text(index)] + " selected")
	
	NetworkPeerController.current_map = Map.maps[$MapList.get_item_text(index)]
	print("network peer controller current map set: " + Map.maps[$MapList.get_item_text(index)])
	_is_start_ready()


func _on_InputDamageMultiplier_text_changed(new_text):
	if not Validator.is_float(new_text):
		$LabelDamageMultiplier/InputDamageMultiplier.text = ""
	_is_start_ready()
