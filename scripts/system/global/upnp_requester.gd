extends Node

var upnp
var is_supported = null
var is_active = false

var current_port = null

var logger = null

var pending_log = ""

func _init():
	# var init
	upnp = UPNP.new()
	# var init end

func _ready():
	activate()

func _process(delta):
	# append pending log to log screen
	if logger != null:
		if pending_log != "":
			logger.append(pending_log)
			pending_log = ""

func _exit_tree():
	deactivate_port_forwarding(current_port)

func activate():
	if is_supported == null:
		if upnp.discover(2000, 2, "InternetGatewayDevice") == UPNP.UPNP_RESULT_SUCCESS:
			is_supported = true
			print("upnp requester activated")
		else:
			is_supported = false
			print("unable to discover internet gateway decive, upnp feature disabled")
	elif not is_supported:
		print("cannot activate upnp requester, upnp feature disabled")
	else:
		print("upnp requester has already activated")

func activate_port_forwarding(port):
	if is_supported and not is_active:
		if upnp.add_port_mapping(int(port), 0, "", "UDP", 0) == UPNP.UPNP_RESULT_SUCCESS:
			print("forwarding udp port: " + str(port))
		else:
			pending_log = "upnp activation failed"
			print("cannot forwarding udp port: " + str(port))
			
		if upnp.add_port_mapping(int(port), 0, "", "TCP", 0) == UPNP.UPNP_RESULT_SUCCESS:
			print("forwarding tcp port: " + str(port))
		else:
			pending_log = "upnp activation failed"
			print("cannot forwarding tcp port: " + str(port))
		
		if pending_log == "":
			pending_log = "upnp activated"
		is_active = true
		
		current_port = port
	else:
		print("cannot activate port forwarding, upnp feature disabled")
	
func deactivate_port_forwarding(port):
	if is_supported and is_active:
		if upnp.delete_port_mapping (int(port), "UDP") == UPNP.UPNP_RESULT_SUCCESS:
			print(str(port) + " udp port forwarding deleted")
		else:
			print("cannot delete udp port forwarding: " + str(port))
			
		if upnp.delete_port_mapping (int(port), "TCP") == UPNP.UPNP_RESULT_SUCCESS:
			print(str(port) + " tcp port forwarding deleted")
		else:
			print("cannot delete tcp port forwarding: " + str(port))
			
		is_active = false
	else:
		print("cannot deactivate port forwarding, upnp feature disabled")
