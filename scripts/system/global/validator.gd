extends Node

var number_regex
var float_regex

func _init():
	number_regex = RegEx.new()
	float_regex = RegEx.new()

func _ready():
	number_regex.compile("^[0-9]*$")
	float_regex.compile("^[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)$")
	print("validator ready")

func is_number(input: String):
	if input == null or input == "":
		return false
	elif number_regex.search(input):
		return true
	else:
		return false

func is_float(input: String):
	if input == null or input == "":
		return false
	elif float_regex.search(input):
		return true
	else:
		return false
