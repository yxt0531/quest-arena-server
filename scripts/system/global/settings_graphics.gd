extends Node

func _ready():
	_center_window()
	
func _center_window():
	OS.set_window_position((OS.get_screen_size() - OS.get_window_size()) / 2)
	print("window centered")
