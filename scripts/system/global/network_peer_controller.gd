extends Node

var port = 53652
var max_player = 8
var current_map = null
var logging = true

var peer = null
var players = {}

var logger = null

# for saving
const SAVE_PATH = "user://network_peer_controller.setting"

var save_file
var settings = {}

var damage_multiplier = 1

var pending_log = ""

func _init():
	save_file = File.new()
	_load()
	
func save():
	settings = {
		"port": port,
		"max_player": max_player,
		"current_map": current_map,
		"logging": logging,
		"damage_multiplier": damage_multiplier
	}
	
	save_file.open(SAVE_PATH, File.WRITE)
	save_file.store_string(to_json(settings))
	save_file.close()

func _load():
	if save_file.file_exists(SAVE_PATH):
		save_file.open(SAVE_PATH, File.READ)
		settings = parse_json(save_file.get_line())
		save_file.close()
		

		if settings.has_all(["port", "max_player", "current_map", "logging", "damage_multiplier"]):
			port = settings.port
			max_player = settings.max_player
			current_map = settings.current_map
			logging = settings.logging
			damage_multiplier = settings.damage_multiplier
		else:
			save()

func start():
	save()
	
	peer = NetworkedMultiplayerENet.new()
	peer.create_server(port, max_player)
	get_tree().set_network_peer(peer)

	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
	
	print("server started")
	print("port: " + str(port))
	print("max player: " + str(max_player))
	print("current map: " + str(current_map))
	
	pending_log = "server started with port: " + str(port)
	
func _process(_delta):
	if peer != null:
		for player_id in players.keys():
			var enemies = players.duplicate()
			enemies.erase(player_id)
			rpc_unreliable_id(player_id, "push_enemies", enemies)
	
	# append pending log to log screen
	if logger != null:
		if pending_log != "":
			logger.append(pending_log)
			pending_log = ""

func _player_connected(id):
	print("player connected: " + str(id))
	
	rset("damage_multiplier", damage_multiplier)
	rpc_id(id, "load_map", current_map)
	
	if logger != null:
		logger.append("player connected: " + str(id))
	
func _player_disconnected(id):
	print("player disconnected: " + str(id))

	players.erase(id)
	
	if logger != null:
		logger.append("player disconnected: " + str(id))

remote func push_player(player):
	players[get_tree().get_rpc_sender_id()] = player

remote func boardcast_fire_packet_received(packet):
	packet.id = get_tree().get_rpc_sender_id()
	
	for player_id in players.keys():
		if player_id != packet.id:
			rpc_id(player_id, "fire_packet_received", packet)
			
	if logger != null:
		logger.append("fire packet received from player: " + str(packet.id))

remote func boardcast_kill_received(nickname, death_position):
	var killed_player_id = get_tree().get_rpc_sender_id()

	for player_id in players.keys():
		if player_id != killed_player_id:
			rpc_id(player_id, "kill_received", nickname, death_position)
	
	if logger != null:
		logger.append("kill received from player: " + str(killed_player_id))
		logger.append("nickname: " + str(nickname))
		logger.append("death position: " + str(death_position))

remote func boardcast_wps_deactivation_received(wps_name):
	var boardcaster_id = get_tree().get_rpc_sender_id()
	for player_id in players.keys():
		if player_id != boardcaster_id:
			rpc_id(player_id, "wps_deactivation_received", wps_name)
			
	if logger != null:
		logger.append("wps deactivation received from player: " + str(boardcaster_id))
		logger.append("wps name: " + str(wps_name))

func disconnect_player(id):
	if players.has(id):
		peer.disconnect_peer(id, true)
		if logger != null:
			logger.append(str(id) + " disconnected by server")

func map_changed():
	for player_id in players.keys():
		rpc_id(player_id, "load_map", current_map)
		print("map changed, push remote load: " + current_map)
